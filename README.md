# VIDEOYA

Videoya is a website that allows you to create video from a png/jpeg/gif and an audio.

Link: https://videoya.fly.dev

## Features

- Import local images or audio
- Select wich part of the audio to include
- Play/Download the generated video

## Tech

- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework

## Installation
```sh
cd videoya
npm i
npm run start
```
