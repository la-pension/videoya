const TEN_MB = 10485760;
let video = document.querySelector('#video-player');
let audio = document.querySelector('#audio');
let start = document.querySelector('#start');
let end = document.querySelector('#end');
let form = document.forms.namedItem('files');
let inputFiles = document.querySelectorAll('.input-file');

form.addEventListener('submit', (e) => {
  let formData = new FormData(form);

  let req = new XMLHttpRequest();
  req.open("POST", "/buildVideo", true);
  req.onload = (event) => {
    if (req.status === 200 || req.status === 206) {
      const data = JSON.parse(req.response);
      let video = document.getElementById("video-player");
      video.setAttribute("src", '/getVideo/' + data.data.name);
      console.log('SENT ' + req.status);
    } else {
      console.log('ERROR ' + req.status);
    }
  }
  req.send(formData);
  e.preventDefault();
});  

const setRangeInterval = (e) => {
  let file = e.currentTarget.files[0] || null;
  if (file) {
    let reader = new FileReader();
    reader.onload = (event) => {
      let media = new Audio(reader.result);
      media.addEventListener('loadedmetadata', () => {
        start.setAttribute("min", 0);
        start.setAttribute("max", media.duration - 1);
        end.setAttribute("min", 1);
        end.setAttribute("max", media.duration);
        start.value = 0;
        end.value = media.duration;
      })
    }

    reader.readAsDataURL(file);
  } else {
    console.log('ERROR: no file on change ???')
  }
}

start.addEventListener('change', () => {
  end.setAttribute("min", parseInt(start.value) + 1);
});

inputFiles.forEach(input => {
  // Same CSS class for submit button
  if (input.id === 'submit')
    return;

  let label = input.nextElementSibling,
    labelContent = label.innerHTML;

  input.addEventListener('change', (e) => {

    if (input.files[0].size > TEN_MB) {
      console.log('FILE TOO BIG');
      e.preventDefault();
      return;
    }

    if (input.id === 'audio') {
      if (!input.files[0].type.match('.*mpeg')) {
        console.log('Wrong audio type ' + input.files[0].type);
        e.preventDefault();
        return;
      }
    } else if (input.id === 'bg') {
      if (!input.files[0].type.match('.*jpeg') && !input.files[0].type.match('.*png') && !input.files[0].type.match('.*gif')) {
        console.log('Wrong image type ' + input.files[0].type);
        e.preventDefault();
        return;
      }
    }

    let fileName = '';
    if (input.files && input.files.length > 1) {
      fileName = (input.getAttribute('data-multiple-caption') || '').replace('{count}', input.files.length);
    } else {
      fileName = e.target.value.split('\\').pop();
    }

    if (fileName) {
      label.innerHTML = fileName;
    } else {
      label.innerHTML = labelContent;
    }
    if (input.id === "audio") {
      setRangeInterval(e);
    }
  })
})
