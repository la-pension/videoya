const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fs = require('fs');
const multer  = require('multer');
const os = require('os');
const upload = multer({ dest: os.tmpdir() });
//const _ = require('lodash');

const apiUtils = require("./src/api_utils");
const { deleteFile } = require("./src/utils");

const port = process.env.PORT || 3000;

const app = express();

if (process.env.CONFIG_ENV != "test") {
  // Discard these deps for testing
  app.set("view engine", "ejs");
  app.use(express.static(__dirname + '/public'));
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(morgan('dev'));
}

const pool = [false, false, false, false, false, false, false, false];

app.get('/', (request, response) => {
  return response.render("index", {
    gifPath: ""
  });
});

app.post('/deleteVideo/:name', (req, res) => {
  const videoPath = path.join(__dirname + `/${req.params.name}.mp4`);
  deleteFile(videoPath);
  res.send({
    status: true,
    message: 'Video is deleted',
  });
});

app.get('/getVideo/:name', (req, res) => {
  const videoPath = path.join(__dirname + `/${req.params.name}.mp4`);
  if (fs.existsSync(videoPath)) {
    const stat = fs.statSync(videoPath);
    const fileSize = stat.size;
    const range = req.headers.range;
    if (range) {
      apiUtils.sendVideoInChunk(videoPath, fileSize, range, res);
    } else {
      apiUtils.sendFullVideo(videoPath, fileSize, res);
    }
  } else {
    res.status(404).send('File not found');
  }
})

app.post('/buildVideo', upload.any(), (req, res) => {
  const id = setInterval(() => {
    const idx = pool.findIndex((elt) => !elt);
    if (idx >= 0) {
      clearInterval(id);
      try {
        if (!req.files || req.files.length !== 2) {
          res.status(500).send({
            message: 'No file uploaded'
          });
        } else {
          bgIdx = req.files.findIndex((elt) => elt.fieldname === "bg");
          audioIdx = req.files.findIndex((elt) => elt.fieldname === "audio");
          pool[idx] = true;
          apiUtils.buildVideo(req.files[bgIdx] || null, req.files[audioIdx], req.body, res, () => {
            pool[idx] = false;
          });
        }
      } catch (err) {
        console.log(err)
        res.status(500).send(err);
      }
    }
  }, 50)
})

if (process.env.CONFIG_ENV != "test") {
  app.listen(port, () => {
    console.log(`Videoya listening on port ${port}`);
  })
}

module.exports = app;
