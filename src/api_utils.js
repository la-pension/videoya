const fs = require('fs');
const path = require('path');
const os = require('os');

const gifToVideo = require("./gif_to_video");
const imgToVideo = require("./img_to_video");

const { deleteFile } = require("./utils");

exports.sendFullVideo = function (path, fileSize, response) {
  const head = {
    'Content-Length': fileSize,
    'Content-Type': 'video/mp4'
  };
  response.writeHead(200, head);
  const file = fs.createReadStream(path).pipe(response);
  file.once('close', () => {
    console.log("Delete " + path)
    deleteFile(path);
  });
}

exports.sendVideoInChunk = function(path, fileSize, range, response) {
  const parts = range.replace(/bytes=/, "").split("-");
  const start = parseInt(parts[0], 10);
  const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
  const chunkSize = (end - start) + 1;
  const file = fs.createReadStream(path, {start, end});
  const head = {
    'Content-Range': `bytes ${start}-${end}/${fileSize}`,
    'Accept-Ranges': `bytes`,
    'Content-Length': chunkSize,
    'Content-Type': 'video/mp4'
  }
  response.writeHead(206, head);
  file.pipe(response);
  file.once('close', () => {
    console.log("Delete " + path)
    deleteFile(path);
  });
}

exports.buildVideo = function(background, audio, body, response, poolCb) {
  const videoName = (Math.random() + 1).toString(36).substring(2);
  const videoPath = path.join(`${os.tmpdir()}/${videoName}.mp4`);     

  const start = body.start;
  const end = body.end;

  const type = background.mimetype.split('/')[1];

  if (type === 'gif') {
    gifToVideo(background.path, audio.path, videoPath, start, end, () => {
      response.status(200).send({
        message: 'Video successfully generated',
        data: {
          name: videoName
        }
      });
      deleteFile(background.path);
      deleteFile(audio.path);
      poolCb();
    }, (e) => {
      console.log(e)
      response.status(500).send({
        message: 'Failed to generate video',
        data: e
      });
      deleteFile(background.path);
      deleteFile(audio.path);
      poolCb();
    });
  } else {
    imgToVideo(background.path, audio.path, videoPath, start, end, () => {
      response.status(200).send({
        message: 'Video successfully generated',
        data: {
          name: videoName
        }
      });
      deleteFile(background.path);
      deleteFile(audio.path);
      poolCb();
    }, (e) => {
      response.status(500).send({
        message: 'Failed to generate video',
        data: e
      });
      deleteFile(background.path);
      deleteFile(audio.path);
      poolCb();
    });
  }
}
