const ffmpegInstaller = require("@ffmpeg-installer/ffmpeg");
const ffprob = require("@ffprobe-installer/ffprobe");

const ffmpeg = require("fluent-ffmpeg");

module.exports = function (gifPath, audioPath, videoPath, start, end, callback, error_callback) {
  if (start < 0) {
    error_callback(new Error("Invalid start time"))
    return;
  }
 
  const command = ffmpeg()
    .setFfprobePath(ffprob.path)
    .setFfmpegPath(ffmpegInstaller.path);

  command
    .input(gifPath)
    .inputOptions(['-ignore_loop 0'])
    .input(audioPath)
    .inputOptions([`-ss ${start}`, `-t ${end - start}`])
    .outputOptions([
      "-shortest",
      "-pix_fmt yuv420p",
      "-c:v libx264",
      "-movflags +faststart",
      "-filter:v crop='floor(in_w/2)*2:floor(in_h/2)*2'",
      `-t ${end - start}`
    ])
    .output(videoPath)
    .on("end", (out, err) => {
      callback();
    })
    .on("error", (e) => {
      error_callback(e)
    })
    .run();
}
