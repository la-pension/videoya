const fs = require('fs')

function deleteFile(path) {
  fs.unlink(path, (err) => {
    if (err) throw err;
  });
}

module.exports = {
  deleteFile
}
