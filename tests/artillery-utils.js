const fs = require('fs');
const os = require('os');
const FormData = require('form-data');

function addMultipartFormData(requestParams, context, ee, next) {
  const formData = {
    bg: fs.createReadStream(__dirname + '/resources/test.gif'),
    audio: fs.createReadStream(__dirname + '/resources/test.mp3')
  };

  requestParams.formData = Object.assign({}, requestParams.formData, formData);
  return next(); 
}

function deleteGeneratedFile(requestParams, response, context, ee, next) {
  fs.readdir(os.tmpdir(), function (err, files) {
    //handling error
    if (err) {
      return console.log('Unable to scan directory: ' + err);
    } 
    //listing all files using forEach
    files.forEach(function (file) {
      if (file.match(/^[a-zA-Z0-9]+\.mp4$/g)) {
        fs.unlink(file, (err) => {
          if (err) console.log(err);
        });
      }
    });
  });
  return next(); // MUST be called for the scenario to continue
}

module.exports = {
  addMultipartFormData: addMultipartFormData,
  deleteGeneratedFile: deleteGeneratedFile
}


