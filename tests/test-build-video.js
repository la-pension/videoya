const assert = require('node:assert');
const { describe, it } = require('node:test');
const fs = require('fs');
const request = require('supertest');
const os = require('os');

const app = require('../server');

const gifPath = `${process.env.PWD}/tests/resources/test.gif`;
const audioPath = `${process.env.PWD}/tests/resources/test.mp3`;
const path = '/buildVideo';

describe('POST /buildVideo', () => {
  it('successful 5 seconds video generated', async () => {
    const res = await request(app)
    .post(path)
    .set('content-type', 'multipart/form-data')
    .field('start', 0)
    .field('end', 5)
    .attach('bg', gifPath)
    .attach('audio', audioPath)
    .expect(200);

    assert.strictEqual(res.body.message, 'Video successfully generated');
    fs.unlinkSync(os.tmpdir() + '/' + res.body.data.name + '.mp4');
  });
});
