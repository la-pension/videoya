const fs = require('fs');
const { describe, it } = require('node:test');

const imgToVideo = require("../src/img_to_video");

const imgPath = `${process.env.PWD}/tests/resources/test.jpg`;
const audioPath = `${process.env.PWD}/tests/resources/test.mp3`;
const videoPath = `${process.env.PWD}/test.mp4`;

describe('Function imgToVideo', () => {
  it('5 seconds video generated', () => {
    return new Promise((resolve, reject) => {
      imgToVideo(imgPath, audioPath, videoPath, 0, 5,
      () => {
        if (!fs.existsSync(videoPath))
          reject(new Error('Failed to generate video'))

        fs.unlinkSync(videoPath);
        resolve("ok")
      }, (e) => {
        reject(new Error('Error while generating video'))
      });
    });
  });

  it('img file not found', () => {
    return new Promise((resolve, reject) => {
      imgToVideo("wrong_file.png", audioPath, videoPath, 0, 5,
      () => {
        fs.unlinkSync(videoPath);
        reject('Video generated successfully')
      }, (e) => {
        resolve("ok")
      });
    });
  });

  it('audio file not found', () => {
    return new Promise((resolve, reject) => {
      imgToVideo(imgPath, "wrong_file.mp3", videoPath, 0, 5,
      () => {
        fs.unlinkSync(videoPath);
        reject('Video generated successfully')
      }, (e) => {
        resolve("ok")
      });
    });
  });

  it('negative start time', () => {
    return new Promise((resolve, reject) => {
      imgToVideo(imgPath, audioPath, videoPath, -1, 5,
      () => {
        fs.unlinkSync(videoPath);
        reject('Video generated successfully')
      }, (e) => {
        resolve("ok")
      });
    });
  });

  it('negative end time', () => {
    return new Promise((resolve, reject) => {
      imgToVideo(imgPath, audioPath, videoPath, 1, -5,
      () => {
        fs.unlinkSync(videoPath);
        reject('Video generated successfully')
      }, (e) => {
        fs.unlinkSync(videoPath);
        resolve("ok")
      });
    });
  });

  it('negative start time and end time', () => {
    return new Promise((resolve, reject) => {
      imgToVideo(imgPath, audioPath, videoPath, -1, -5,
      () => {
        fs.unlinkSync(videoPath);
        reject('Video generated successfully')
      }, (e) => {
        resolve("ok")
      });
    });
  });
});
